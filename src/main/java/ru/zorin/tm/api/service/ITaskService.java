package ru.zorin.tm.api.service;

import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    void load(List<Task> tasks);

    List<Task> findAll(String userId);

    List<Task> getTaskList();

    void clear(String userId);

    Task findOneById(String userId, String id);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    Task removeOneByIndex(String userId, Integer index);

    Task removeOneById(String userId, String id);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);
}
