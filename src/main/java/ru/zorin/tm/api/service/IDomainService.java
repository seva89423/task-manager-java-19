package ru.zorin.tm.api.service;

import ru.zorin.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);
}