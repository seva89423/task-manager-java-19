package ru.zorin.tm.api.service;

import ru.zorin.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getTermCommands();
}