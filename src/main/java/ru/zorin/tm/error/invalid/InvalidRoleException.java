package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidRoleException extends AbstractException {

    public InvalidRoleException() {
        super("Role is invalid");
    }
}