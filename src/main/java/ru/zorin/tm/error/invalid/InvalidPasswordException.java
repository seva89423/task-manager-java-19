package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidPasswordException extends AbstractException {

    public InvalidPasswordException() {
        super("Invalid password");
    }
}