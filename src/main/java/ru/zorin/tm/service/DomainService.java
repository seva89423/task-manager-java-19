package ru.zorin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.service.*;
import ru.zorin.tm.dto.Domain;

public class DomainService implements IDomainService {

    private final IUserService userService;

    private final ITaskService taskService;

    private final IProjectService projectService;

    public DomainService(IUserService userService, ITaskService taskService, IProjectService projectService) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void load(Domain domain) {
        if (domain == null) return;
    }

    @Nullable
    @Override
    public void export(@Nullable Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getProjectList());
        domain.setTasks(taskService.getTaskList());
        domain.setUsers(userService.getUsersList());
    }
}