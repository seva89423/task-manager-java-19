package ru.zorin.tm.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.zorin.tm.role.Role;

import java.io.Serializable;
@Getter
@Setter
@Data
public class User extends AbstractEntity implements Serializable {

    private String login;

    private String password;

    private String email;

    private String firstName;

    private String lastName;

    private String middleName;

    private Role role = Role.USER;

    private Boolean locked = false;

    public String toString() {
        return getId()  +": "+login;
    }
}