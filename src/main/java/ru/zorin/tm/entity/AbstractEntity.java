package ru.zorin.tm.entity;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public abstract class AbstractEntity {

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }
}