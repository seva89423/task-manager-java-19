package ru.zorin.tm.entity;

import java.io.Serializable;
import java.util.UUID;

public class Task extends AbstractEntity implements Serializable {

    private String name;

    private String description;

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getId()  +": "+name;
    }
}