package ru.zorin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.role.Role;

import java.util.List;

public class ProjectShowListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "show-project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show list of projects";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        int index = 1;
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (Project project : projects) System.out.println(index +". "+ project);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}