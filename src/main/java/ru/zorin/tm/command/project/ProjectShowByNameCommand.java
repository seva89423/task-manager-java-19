package ru.zorin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "show-project-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project using name";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}