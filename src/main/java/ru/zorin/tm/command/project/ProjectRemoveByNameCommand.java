package ru.zorin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-project-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project using name";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        else System.out.println("[COMPLETE]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}