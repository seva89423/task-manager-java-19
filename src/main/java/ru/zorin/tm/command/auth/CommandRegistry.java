package ru.zorin.tm.command.auth;

import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.util.TerminalUtil;

public class CommandRegistry extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "registry";
    }

    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        System.out.println("[COMPLETE]");
    }
}