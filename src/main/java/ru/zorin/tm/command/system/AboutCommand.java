package ru.zorin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Show info about developer";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
    }
}