package ru.zorin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {


    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "remove-user-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete user using id";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeById(userId, id);
        if (user == null) throw new InvalidLoginException();
        System.out.println("[REMOVED]");
        serviceLocator.getAuthService().logout();
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}