package ru.zorin.tm.command.data.fastxml.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.role.Role;

import java.io.File;
import java.nio.file.Files;

public class DataJsonFastClearCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-fast-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from json (FasterXML) file";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (FAST) DELETE]");
        final File file = new File(DataConst.FILE_JSON_FAST);
        Files.deleteIfExists(file.toPath());
        System.out.println("[COMPLETE]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}
