package ru.zorin.tm.command.data.bin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.role.Role;

import java.io.*;

public class DataBinLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from bin file";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_BIN);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        fileInputStream.close();
        System.out.println("[COMPLETE]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}