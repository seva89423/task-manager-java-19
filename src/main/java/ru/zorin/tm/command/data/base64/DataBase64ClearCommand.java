package ru.zorin.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.role.Role;

import java.io.File;
import java.nio.file.Files;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from base64 file";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
    System.out.println("[DATA BASE64 DELETE]");
    final File file = new File(DataConst.FILE_BASE64);
    Files.deleteIfExists(file.toPath());
    System.out.println("[DATA IS CLEAR]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}