package ru.zorin.tm.command.data.jaxb.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.role.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public class DataXmlSaveCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-jaxb-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml (Jax-B) file";
    }

    @Nullable
    @Override
    public void execute() throws IOException, JAXBException {
        System.out.println("[DATA XML SAVE]");
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        final File file = new File(DataConst.FILE_XML_FAST);
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
        System.out.println("[COMPLETE]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}