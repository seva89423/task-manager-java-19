package ru.zorin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.role.Role;

public class TaskClearCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear list of task";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR TASKS]");
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}