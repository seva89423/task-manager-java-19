package ru.zorin.tm.command;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.service.ServiceLocator;
import ru.zorin.tm.role.Role;

public abstract class AbstractCommand {

    @Setter
    protected ServiceLocator serviceLocator;

    public AbstractCommand() {

    }

    public Role[] roles(){
        return null;
    }

    public abstract String arg();

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String description();

    @NotNull
    public abstract void execute() throws Exception;
}