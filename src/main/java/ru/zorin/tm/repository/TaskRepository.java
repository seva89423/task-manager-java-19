package ru.zorin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, Task task){
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, Task task){
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        tasks.remove(task);
    }
    @Override
    public List<Task> getTaskList() {
        return tasks;
    }

    @Override
    public void add(@NotNull final List<Task> task) {
        for (final Task tas : task) {
            if (tas == null) return;
            tasks.add(tas);
        }
    }

    @Override
    public void load(final List<Task> tasks) {
        add(tasks);
    }

    @Override
    public List<Task> findAll(final String userId){
        final List<Task> result = new ArrayList<>();
        for (Task task:tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final String userId){
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        this.tasks.clear();
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        return tasks.get(index);
    }

    @Override
    public Task getTaskByName(String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        for (final Task task:tasks){
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Task task = getTaskByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskEmptyException();
        remove(userId, task);
        return task;
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        for (final Task task:tasks){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskEmptyException();
        tasks.remove(task);
        return task;
    }
}
