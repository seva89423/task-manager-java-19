package ru.zorin.tm.repository;

import lombok.SneakyThrows;
import ru.zorin.tm.api.repository.IUserRepository;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.invalid.InvalidIdException;
import ru.zorin.tm.error.invalid.InvalidLoginException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String name) {
        for (User user : users) {
            if (name.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(User user) {
        users.remove(user);
        return null;
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        final User user = findById(id);
        if (user == null) return null;
        removeUser(user);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        final User user = findByLogin(login);
        if (user == null) return null;
        removeUser(user);
        return user;
    }

    @Override
    public List<User> getUsersList() {
        return users;
    }

    @Override
    public void add(final List<User> userList) {
        for (final User user : userList) {
            if (user == null) return;
            users.add(user);
        }
    }

    @Override
    public void load(final List<User> userList) {
        add(userList);
    }
}